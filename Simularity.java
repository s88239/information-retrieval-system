public class Simularity{
	private String id;
	private int position;
	private double inner_product, sigma_square_product_w1, sigma_square_product_w2;

	public Simularity(){
		inner_product = 0;
		sigma_square_product_w1 = 0;
		sigma_square_product_w2 = 0;
		id = "";
		position = 0;
	}
	public Simularity(String _id, int _position){
		this();
		id = _id;
		position = _position;
	}

	public void addSim(double w1, double w2){
		inner_product = inner_product + w1 * w2;
	}
	public void setSquareWeight(double w1, double w2){
		sigma_square_product_w1 = w1;
		sigma_square_product_w2 = w2;
	}
	public String getId(){
		return id;
	}
	public int getPosition(){
		return position;
	}
	public double getSim(){
		//System.out.println("Inner = "+inner_product+"; sigma w1 = "+sigma_square_product_w1+"; sigma w2 = "+sigma_square_product_w2+"; sqrt = "+Math.sqrt( sigma_square_product_w1 * sigma_square_product_w2 ) );
		if(sigma_square_product_w1==0||sigma_square_product_w2==0){
			return 0;
		}
		return inner_product / Math.sqrt( sigma_square_product_w1 * sigma_square_product_w2 );
	}
	public String toString(){
		return "Inner = "+inner_product+"; sigma w1 = "+sigma_square_product_w1+"; sigma w2 = "+sigma_square_product_w2+"; sqrt = "+Math.sqrt( sigma_square_product_w1 * sigma_square_product_w2 ) ;
	}
}
