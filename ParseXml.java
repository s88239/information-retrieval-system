import java.io.*;
import java.util.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
 
public class ParseXml{
	private static final String xml_tag_name= "topic";
	private static final String xml_tag_number = "number";
	private static final String[] xml_tag_list = {"title","concepts"};
	private DocInfo[] doc_array;
	private String file_path;
	
	public ParseXml(){
		file_path = "../query/query-train.xml";
	}
	public ParseXml(String _file_path){
		file_path = _file_path;
	}
	
	public DocInfo[] getDoc(){
		return doc_array;
	}
	public void parse(){
		String line, id_name;
		Parser parser;
		ArrayList<String> doc_key_array;
    	try {
	 		File fXmlFile = new File( file_path );
			DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName(xml_tag_name);

			doc_array = new DocInfo[ nList.getLength() ]; // create an array to store the docInfo of all topics
			for (int i = 0; i < nList.getLength(); ++i) { // do in all topics
				Node nNode = nList.item(i); // i_th topic
				//System.out.println("\n" + (i+1) + ". Current Element :" + nNode.getNodeName());
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					id_name = eElement.getElementsByTagName(xml_tag_number).item(0).getTextContent(); // get the text in the <number> tag
					parser = new Parser( id_name.substring(id_name.length()-3) ); // create new parser to parsing the new topic in the XML file
					for(String the_name : xml_tag_list){
						line = eElement.getElementsByTagName(the_name).item(0).getTextContent(); // get the text in other tags
						//System.out.println(line);
						parser.parse(line); // parse the text
					}
					doc_array[i] = parser.getDoc();
				}
			}
			/*for(int j=0; j<doc_array.length; ++j){ // debug: print the content in DocInfo
				System.out.println(doc_array[j]);
			}*/
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
