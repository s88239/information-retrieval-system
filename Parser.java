import java.util.*;
import java.io.*;

public class Parser{
	public static final int n_parsed = 2; // n-gram
	public static final char[] stop_words = {'一','不','之','也','了','了','人','他','你','個','們','在','就','我','是','有','的','而','要','說','這','都'};
	private DocInfo doc; // store every word string and its times in the DocInfo

	public Parser(){
		doc = new DocInfo();
    }
	public Parser(String _id){
		doc = new DocInfo(_id);
	}

	public DocInfo getDoc(){
		return doc;
	}

	public void parse(String line){ // do parsing
        char word;
        String str = "";
        int i;
        boolean letterFlag = false, httpFlag = false;
        for( i = 0 ; i < line.length() ; ++i ){
            word = line.charAt( i );
            if( httpFlag ){ // concate the url
                if( isAphabet(word) || isPunct(word) ){
                    str = str + word;
                    continue;
                }
                else{
                    insertKey( str );
                    str = "";
                    httpFlag = false;
                }
            }
            if( isAphabet(word) ){ // the character is an English letter or digit
                //System.out.println( "letter:" + word );
                str = str + word; // concatenate the string
                letterFlag = true;
            }
            else{
                if( letterFlag ){ // the previous word is a letter, and this word isn't. Get the complete word.
                    if( str.equalsIgnoreCase("http") ){
                        httpFlag = true;
                        continue;
                    }
                    insertKey( str );
                }
                if( !isPunct(word) && !isStopWord(word) ){ // it's not a letter, space or punctuation
                    str = Character.toString( word ); // convert char to string
                    //if(!isStopWord(word)) insertKey( str );
                    concatenate( line, i, str, 1); // concatenate the string of Chinese at most "n_parsed" words
                }
                str = ""; // initialized string
                letterFlag = false; // remove flag because this word isn't a letter
                httpFlag = false;
            }
        }
        if( letterFlag ) insertKey( str ); // the end of line which does not insert yet.
    }
    private void insertKey( String str ){
		//System.out.println("Insert:" + str);
		doc.increaseTF(str);
    }
    private void concatenate( String str, int idx, String word, int times ){ // concatenate continuous chinese word at most "times" words
        if( times >= n_parsed || idx+1 >= str.length() ) return;
        char nextChar = str.charAt(idx+1);
        if( !isAphabet(nextChar) && !isPunct(nextChar) && !isStopWord(nextChar) ){
            String newStr = word + nextChar;
            insertKey( newStr );
            concatenate( str, idx+1, newStr, ++times);
        }
    }
    public static boolean isPunct(final char c) { // judge the character is a punctuation or special symbol
        final int val = (int)c;
        //System.out.print(c+":"+val+" ");
        return Character.isSpaceChar(c)
        || val >= 0 && val <= 47
        || val >= 58 && val <= 64
        || val >= 91 && val <= 96
        || val >= 0x7B && val <= 0xA0 // Gabled
        || val >= 0x2000 && val <= 0x28FF // a variety of symbols
        || val >= 0x3000 && val <= 0x303F // CJK Symbols and Punctuation
        || val >= 0xFE30 && val <= 0xFE4F // CJK Compatibility Forms
        || val >= 0xFF00 && val <= 0xFFEF; // Halfwidth and Fullwidth Forms
    }
    public static boolean isAphabet(final char word){ // judege the character is a letter or not
        return Character.isDigit(word) || Character.isLowerCase(word) || Character.isUpperCase(word);
    }
	public static boolean isStopWord(final char word){ // judege the character is the stop word or not
		for(char c : stop_words){
			if(c==word) return true;
		}
		return false;
	}
}
