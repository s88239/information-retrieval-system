import java.util.*;

public class TermInfo{
	private int TF;
	private double IDF;
	private double TFIDF;
	private double feedback_weight;

	public TermInfo(){
		TF = 0;
		IDF = 1;
		TFIDF = 0;
		feedback_weight = 0;
	}
	public TermInfo(int _TF){
		this();
		TF = _TF;
	}
	public TermInfo(int _TF, double _IDF){
		this(_TF);
		IDF = _IDF;
	}

	public void setTF(int times){
		TF = times;
	}
	public void setIDF(double _IDF){
		IDF = _IDF;
	}
	public void setTFIDF(double _TFIDF){
		TFIDF = _TFIDF;
	}
	public void increaseTF(){
		TF += 1;
	}
	public void increaseTF(int times){
		TF += times;
	}
	public void increaseFeedback(double w){
		feedback_weight += w;
	}
	public void mixTFIDF(){
		double alpha = 0.5;
		//System.out.println(alpha+"*"+TFIDF+"+"+feedback_weight+"="+ (alpha*TFIDF+feedback_weight) );
		TFIDF = alpha * TFIDF + feedback_weight;
		feedback_weight = 0;
	}
	public int getTF(){
		return TF;
	}
	public double getIDF(){
		return IDF;
	}
	public double getTFIDF(){
		return TFIDF;
	}
	public String toString(){
		return "{TF:" + TF + ", IDF:" + IDF + "}";
	}
}
