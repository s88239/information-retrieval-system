import java.util.*;
import java.io.*;

public class IR{
	private Dataset database_object;
	private ParseXml xml_object;
	private DocInfo[] database_doc;
	private DocInfo[] xml_doc;
	private double[] w1_square;
	private double[] w2_square;
	private boolean isFeedback;
	private String query_file;
	private String rank_list;
	private String model_dir;
	private String NTCIR_dir;

	public IR(){
		isFeedback = false;
		query_file = "../query/query-train.xml";
		rank_list = "rank.list";
		model_dir = "../model/";
		NTCIR_dir = "../CIRB010/";
	}
	
	public void process(String[] param){
		for(int i = 0; i < param.length; ++i){
			if( param[i].equals("-r") ){
				isFeedback = true;
			}
			else if( param[i].equals("-i") ){
				query_file = param[++i];
			}
			else if( param[i].equals("-o") ){
				rank_list = param[++i];
			}
			else if( param[i].equals("-m") ){
				model_dir = param[++i];
				if( model_dir.charAt(model_dir.length()-1) != '/' )
					model_dir = model_dir + '/';
			}
			else if( param[i].equals("-d") ){
				NTCIR_dir = param[++i];
				if( NTCIR_dir.charAt(NTCIR_dir.length()-1) != '/' )
					NTCIR_dir = NTCIR_dir + '/';
			}
			else{
				System.out.println("Invalid value \"" + param[i] + "\" due to undefined option");
			}
		}
		System.out.println("query = "+query_file +"\nrank = "+rank_list+"\nmodel = "+model_dir+"\nNTCIR = "+NTCIR_dir);

		// do xml file parsing
		xml_object = new ParseXml(query_file);
		xml_object.parse();
		xml_doc = xml_object.getDoc();

		// do database file parsing
		database_object = new Dataset(model_dir);
		database_object.establish();
		database_doc = database_object.getDoc();

		ArrayList<Simularity> sim_list;
		PrintWriter outputStream = null;
		w1_square = new double[xml_doc.length];
		w2_square = new double[database_doc.length];
		for(int i = 0; i < xml_doc.length; ++i){ // calculate the sigma for square of w1
			w1_square[i] = 0;
			for( String key_element : xml_doc[i].getTermsName() ){
				w1_square[i] += Math.pow( xml_doc[i].getTFIDF(key_element), 2);
			}
		}
		for(int j = 0; j < database_doc.length; ++j){ // calculate the sigma for square of w2
			w2_square[j] = 0;
			for( String key_element : database_doc[j].getTermsName() ){
				w2_square[j] += Math.pow( database_doc[j].getTFIDF(key_element), 2);
			}
		}
		try{
			outputStream = new PrintWriter( new FileOutputStream(rank_list) );
			for(int i = 0; i < xml_doc.length; ++i){
				sim_list = calSimRank(i); // get the rank of simularity
				if(isFeedback){
					System.out.println("Feedbacking..");
					double beta = 0.5, gamma = 0.5;
					int doc_position, rel_num = 5, non_rel_num = 5;
					for(int j = 0; j < rel_num; ++j){
						doc_position = sim_list.get(j).getPosition();
						//System.out.println("["+doc_position+"] rel");
						for( String s : database_doc[ doc_position ].getTermsName() ){
							//System.out.println( s + ": TFIDF = " + database_doc[ doc_position ].getTFIDF(s) + ", rel_feedback = " + database_doc[ doc_position ].getTFIDF(s) * beta / rel_num );
							xml_doc[i].increaseFeedback(s, database_doc[ doc_position ].getTFIDF(s) * beta / rel_num );
						}
					}
					for(int j = 0; j < rel_num; ++j){
						doc_position = sim_list.get(sim_list.size()-1 - j).getPosition();
						//System.out.println("["+doc_position+"] non-rel");
						for( String s : database_doc[ doc_position ].getTermsName() ){
							//System.out.println( s + ": TFIDF = " + database_doc[ doc_position ].getTFIDF(s) + ", non_rel_feedback = " + database_doc[ doc_position ].getTFIDF(s) * gamma / non_rel_num );
							xml_doc[i].increaseFeedback(s,  -1 * database_doc[ doc_position ].getTFIDF(s) * gamma / non_rel_num );
						}
					}
					for(String s : xml_doc[i].getTermsName()){
						System.out.println("Mix " + s);
						xml_doc[i].mixTFIDF(s);
					}
					sim_list = calSimRank(i);
				}
				System.out.println("Writing " + xml_doc[i].getId() + " to file...");
				for(int j=0; j < 100; ++j){
					if( j >= sim_list.size() ) break;
					String[] doc_id = sim_list.get(j).getId().split("/");
					outputStream.println( xml_doc[i].getId() + " " + doc_id[doc_id.length - 1].toLowerCase() );// write the ranking list to the file
					System.out.println(sim_list.get(j).getId() + " " + sim_list.get(j).getSim() );
				}
			}
		} catch (FileNotFoundException e){
			System.out.println("Error opening the ranking file.");
			System.exit(0);
		}
		System.out.println("Done!");
		outputStream.close();
	}
	private void sortBySim( ArrayList<Simularity> sim_list ){
        // sort list for simularity
        Collections.sort(sim_list, new Comparator<Simularity>(){
            @Override
            public int compare(Simularity sA, Simularity sB){
                // sort by decending
                if( sA.getSim() - sB.getSim() < 0 ){
                    return 1;
                } else if( sA.getSim() - sB.getSim() > 0 ){
                    return -1;
                }
                return 0;
            }
        });
    }
	private ArrayList<Simularity> calSimRank(int query_index){
		ArrayList<Simularity> sim_list = new ArrayList<Simularity>();
		Simularity sim;
		double w1, w2;
		for(int j = 0; j < database_doc.length; ++j){
			sim = new Simularity( database_doc[j].getId(), j );
			for(String key_element : xml_doc[query_index].getTermsName()){
				w1 = xml_doc[query_index].getTFIDF(key_element);
				w2 = database_doc[j].getTFIDF(key_element);
				if( w1 != 0 && w2 != 0){
					sim.addSim( w1, w2 );
				}
				//System.out.println(key_element+":["+query_index+"]"+ xml_doc[query_index].getTFIDF(key_element)+" ["+j+"]"+ database_doc[j].getTFIDF(key_element) );
			}
			sim.setSquareWeight(w1_square[query_index], w2_square[j]);
			if(sim.getSim() != 0) sim_list.add( sim );
		}
		sortBySim( sim_list ); // sort the list for ranking by simularity
		return sim_list;
	}
    public static void main(String[] args){
		IR ir = new IR();
		ir.process(args);
    } 
}
