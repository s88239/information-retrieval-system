import java.util.*;

public class DocInfo{
	private String id;
	private HashMap<String, TermInfo> terms;
	private int total_frequency;

	public DocInfo(){
		id = "-1";
		terms = new HashMap<String, TermInfo>();
		total_frequency = 0;
	}
	public DocInfo(String _id){
		this();
		id = _id;
	}

	public void setTF(String word, int times){
		if( !terms.containsKey(word) ){
			terms.put( word, new TermInfo(times) );
		}
		else{
			terms.get(word).setTF( times );
		}
		total_frequency += times;
	}
	public void setIDF(String word, double w){
		if( !terms.containsKey(word) ){
			terms.put( word, new TermInfo(0, w) );
		}
		else{
			terms.get(word).setIDF( w );
		}
	}
	public void increaseTF(String word){
		if( !terms.containsKey(word) ){
			terms.put( word, new TermInfo(1) );
		}
		else{
			terms.get(word).increaseTF();
		}
		++total_frequency;
	}
	public void increaseFeedback(String word, double feedback_weight){
		if( terms.containsKey(word) ){
			terms.get(word).increaseFeedback(feedback_weight);
		}
	}
	public void mixTFIDF(String word){
		if( terms.containsKey(word) ){
			terms.get(word).mixTFIDF();
		}
	}
	public double getTFIDF(String word){
		if( !terms.containsKey(word) ){
			return 0;
		}
		TermInfo term = terms.get(word);
		double TFIDF;
		if( ( TFIDF = term.getTFIDF() ) == 0 ){
			TFIDF = (double)term.getTF()/total_frequency * term.getIDF();
			term.setTFIDF(TFIDF);
		}
		return TFIDF;
	}
	public String getId(){
		return id;
	}
	public Set<String> getTermsName(){
		return terms.keySet();
	}
	public String toString(){
		String str = "DocID:" + id + ", total freqency:" + total_frequency + "\n";
		System.out.println(terms);
		return str;
	}
}
