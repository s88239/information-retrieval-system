import java.util.*;
import java.io.*;

public class Dataset{
	private String vocab_addr;
	private String inverted_addr;
	private String file_list_addr;
	private DocInfo[] doc_array;
	private ArrayList<String> vocab_array;
	private ArrayList<String> file_list_array;

	public Dataset(){
		setFilePath("../model/");
		vocab_array = new ArrayList<String>();
		file_list_array = new ArrayList<String>();
	}
	public Dataset(String _dir_path){
		setFilePath(_dir_path);
		vocab_array = new ArrayList<String>();
		file_list_array = new ArrayList<String>();
	}		
	private void setFilePath(String model_dir_addr){
		vocab_addr = model_dir_addr + "vocab.all";
		inverted_addr = model_dir_addr + "inverted-file";
		file_list_addr = model_dir_addr + "file-list";
	}

	public DocInfo[] getDoc(){
		return doc_array;
	}
	public void establish(){
		String line;
		try{
			// open the vocab.all
			FileInputStream vocab_fins = new FileInputStream( vocab_addr );
			InputStreamReader vocab_readStream = new InputStreamReader( vocab_fins , "UTF-8");
            BufferedReader vocab_br = new BufferedReader( vocab_readStream );
			// open the inverted-file
			FileInputStream inverted_fins = new FileInputStream( inverted_addr );
			InputStreamReader inverted_readStream = new InputStreamReader( inverted_fins , "UTF-8");
            BufferedReader inverted_br = new BufferedReader( inverted_readStream );
			// open the file-list
			FileInputStream file_list_fins = new FileInputStream( file_list_addr );
			InputStreamReader file_list_readStream = new InputStreamReader( file_list_fins , "UTF-8");
            BufferedReader file_list_br = new BufferedReader( file_list_readStream );
			
			// read the content from vocab
			while( ( line = vocab_br.readLine() )!= null ){
				vocab_array.add(line);
			}
			//System.out.println("vocab size = " + vocab_array.size() );
			
			// read the content from file-list
			while( ( line = file_list_br.readLine() )!= null ){
				file_list_array.add(line);
			}
			//System.out.println("file-list size = " + file_list_array.size() );

			// construct the array of VSM for each doc
			int file_num = file_list_array.size();
			doc_array = new DocInfo[ file_num ];
			for(int i=0; i < doc_array.length; ++i){
				doc_array[i] = new DocInfo( file_list_array.get(i) );
			}

			String[] row_param, following_row;
			String word, word1, word2;
			// read the content from inverted
			int tmp_count = 0, term_count;
			double IDF_weight;
			while( ( line = inverted_br.readLine() )!= null ){
				row_param = line.split(" "); // split the string of current row
				word1 = vocab_array.get( Integer.parseInt(row_param[0]) );
				int word2_idx = Integer.parseInt(row_param[1]);
				word2 = (word2_idx==-1) ? "" : vocab_array.get( word2_idx );
				word = word1 + word2;
				term_count = Integer.parseInt(row_param[2]);
				IDF_weight = Math.log( (double)file_num/term_count );
				if(tmp_count%20000==0){ // for debugging
					System.out.print("["+tmp_count+"]");
					System.out.println(word + " ~ " + row_param[2]);
				}
				++tmp_count;
				for(int i = 0; i < term_count; ++i){ // set the TF for each term in database
					line = inverted_br.readLine();
					if( word2_idx==-1 ) continue; // prevent unigram
					char c1 = word1.charAt(0), c2 = word2.charAt(0);
					if( !Parser.isAphabet(c1) && Parser.isPunct(c1)
						|| !Parser.isAphabet(c2) && Parser.isPunct(c2)
						|| Parser.isStopWord(c1) || Parser.isStopWord(c2) ){
						continue; // don't parse punct
					}
					
					following_row = line.split(" "); // split the string of current row
					doc_array[ Integer.parseInt(following_row[0]) ].setTF(word, Integer.parseInt(following_row[1]) );
					doc_array[ Integer.parseInt(following_row[0]) ].setIDF(word, IDF_weight );
				}
				//if(tmp_count>50) break;
			}
			//System.out.println(tmp_count);
			vocab_br.close();
			file_list_br.close();
			inverted_br.close();

			// write the the VSM of database to the file
			/*FileOutputStream fos = new FileOutputStream("VSM");
		    ObjectOutputStream oos = new ObjectOutputStream(fos);   
		    oos.writeObject(VSM); // write VSM to ObjectOutputStream
		    oos.close();*/
		} catch(FileNotFoundException fe){ // can't find the file
			System.out.println("Can not open the file!\nDetail:" + fe.getMessage() );
			System.exit(0);
		} catch (Exception e) {
			System.out.println("It seems some error happened!\nDetail:" + e.getMessage() );
			System.exit(0);
		}
		/*for(int i = 0; i < 20; ++i){
			System.out.println(doc_array[100*i+55]);
		}*/
	}
}
